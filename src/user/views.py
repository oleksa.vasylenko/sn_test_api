from rest_framework.permissions import AllowAny
from rest_framework.generics import CreateAPIView

from . import serializers, models


class SignUpView(CreateAPIView):
    queryset = models.User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = serializers.UserSerializer
