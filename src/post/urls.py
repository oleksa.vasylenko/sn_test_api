from django.urls import path

from . import views


urlpatterns = [
    path('', views.PostViewSet.as_view({'get': 'list'})),
    path('make/', views.PostViewSet.as_view({'post': 'create'})),
    path('<int:pk>/', views.PostViewSet.as_view({'get': 'retrieve'})),
    path('<int:pk>/like/', views.like_view)
]
