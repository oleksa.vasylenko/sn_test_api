from django.db.models import Count, Q
from rest_framework.status import HTTP_200_OK
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .models import Post, Like
from .serializers import PostSerializer


class PostViewSet(ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def get_queryset(self):
        qs = Post.objects.annotate(
            likes_count=Count('likes', filter=Q(likes__state=True)))
        qs = qs.select_related('author')
        return self.get_serializer_class().setup_eager_loading(qs)


@api_view(['POST'])
def like_view(request, pk):
    post = Post.objects.get(pk=pk)
    like, created = Like.objects.get_or_create(owner=request.user, post=post)
    if not created:
        like.change_like_state()
    return Response(data={'liked': like.state}, status=HTTP_200_OK)


