from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=256, unique=True)
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    def have_user_liked(self, user):
        for l in self.likes.all():
            if l.state and l.owner == user:
                return True
        return False


class Like(models.Model):
    owner = models.ForeignKey(User,
                              on_delete=models.CASCADE,
                              related_name='owner')
    state = models.BooleanField(default=True)
    post = models.ForeignKey(Post,
                             null=True,
                             on_delete=models.CASCADE,
                             related_name='likes')

    def change_like_state(self):
        self.state = False if self.state else True
        self.save()
        return self.state
