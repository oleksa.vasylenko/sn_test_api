from rest_framework import serializers
from .models import Post


class PostSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    author = serializers.SerializerMethodField()
    likes_count = serializers.IntegerField(read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'author', 'title', 'body', 'likes_count')

    def get_author(self, obj):
        return obj.author.username

    @staticmethod
    def setup_eager_loading(queryset):
        return queryset.prefetch_related('likes__owner')

    def create(self, validated_data):
        req_user = self.context['request'].user
        return self.Meta.model.objects.create(author=req_user, **validated_data)

    def to_representation(self, instance):
        result = super().to_representation(instance)
        req_user = self.context['request'].user
        result['user_liked'] = instance.have_user_liked(req_user)
        return result
