# Social Network API
Simple API for social network. Provides user and post creation, ability to like and authorize via JWT

# How to install
1. Clone repo
2. Create virtual environment (e.g. `mkvirtualenv --python=$(which python3) test_api`)
3. Install dependencies `cd src` and then `pip install -r requirements.txt`

# How to run
`python manage.py runserver`